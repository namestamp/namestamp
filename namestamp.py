#!/usr/bin/env python

#   Namestamp - link Namecoin timestaps to the Bitcoin blockchain.
#   Copyright (C) 2015  Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import binascii
import os
import sys
import time

import coinInterface
import preamble

# REST interface connections.
btc = coinInterface.CoinInterface ("localhost", 8332)
nmc = coinInterface.CoinInterface ("localhost", 8336)

################################################################################

def getScriptDir ():
  """
  Get the script's directory.
  """

  # https://stackoverflow.com/questions/4934806/python-how-to-find-scripts-directory
  return os.path.dirname (os.path.realpath (sys.argv[0]))

def formatTimestamp (ts):
  """
  Format a timestamp to a ISO string.  The string is returned
  according to local time.
  """

  fmt = "%Y-%m-%d %H:%M:%S"
  return time.strftime (fmt, time.localtime (ts))

class NameStamp:
  """
  Main class handling the operation of namestamp.
  """

  def __init__ (self, out, btc, nmc):
    """
    Initialise with the RPC connections.
    """

    self.out = out
    self.btc = btc
    self.nmc = nmc

    # We keep track of all constants in a dictionary.  This allows us
    # to write them as 'c[i]' in the proof code, and only later define
    # the actual values.
    self.constants = []

  def getConstant (self, hexStr):
    """
    Save the hex string in self.constants and return a string to be
    used in the proof instead.  The string will be of the form 'c[i]'.
    """

    if hexStr is None:
      return "None"

    ind = len (self.constants)
    self.constants.append (str (hexStr))

    return "c[%d]" % ind

  def performHash (self, pre, post):
    """
    Write out operation to perform a hash-chain step.  Pre- and postfix
    should be hex strings.  They are saved to self.constants and not
    directly printed.  Instead, they are printed as 'c[i]'.
    """

    preStr = self.getConstant (pre)
    postStr = self.getConstant (post)

    self.out.write ("  h.hash (%s, %s)\n" % (preStr, postStr))
    self.hasher.hash (pre, post)

  def buildKnownValue (self, hexStr):
    """
    Perform a hash that should result in the hash of hexStr.  This function
    assumes that the current hasher's state is a substring of the hexStr.
    We split the string and add prefix and postfix accordingly so that
    after combining and before the performed hash step, we get hexStr.

    This is used at multiple places, for instance, to build the initial
    transaction that contains the name value, or to build block headers
    with the current state as the Merkle root.
    """

    cur = self.hasher.getHex ()
    parts = hexStr.partition (cur)
    if parts[1] != cur:
      raise RuntimeError ("can not build known value from hasher state")

    self.performHash (parts[0], parts[2])

  def contractMerkleTree (self, tx):
    """
    Build the Merkle tree from a list of all transactions in the block
    and contract it.  tx should contain all transactions as hex strings,
    and one of them should match the current hasher state.  In the end,
    the hasher will be set to the Merkle root.
    """

    # We mimic what the Bitcoin code does when building the Merkle tree:
    # Just continue hashing neighbouring elements (halving the array length
    # in each step) until we reach the root.  If we have an odd number of
    # elements at some step, duplicate the last one.  At each step,
    # some element should match the current state.  Use this to
    # perform the Merkle branch operations on the hasher.

    entries = []
    for hexStr in tx:
      entries.append (bytearray (binascii.unhexlify (hexStr)))
    assert len (entries) > 0

    while len (entries) > 1:
      if len (entries) % 2 == 1:
        entries.append (entries[-1])
      assert len (entries) % 2 == 0

      state = self.hasher.get ()

      newEntries = []
      foundState = False
      for i in range (0, len (entries), 2):
        if not foundState:
          if entries[i] == state:
            foundState = True
            self.performHash (None, binascii.hexlify (entries[i + 1]))
          elif entries[i + 1] == state:
            foundState = True
            self.performHash (binascii.hexlify (entries[i]), None)

        newEntries.append (preamble.doubleHash (entries[i] + entries[i + 1]))

      if not foundState:
        raise RuntimeError ("did not find state while constructing Merkle tree")
      entries = newEntries

  def followMerkleBranch (self, index, branch):
    """
    Follow a Merkle branch.  We end up with the root hash in the current state.
    """

    for node in branch:
      if index & 1:
        self.performHash (node, None)
      else:
        self.performHash (None, node)

      index >>= 1

    if index > 0:
      raise RuntimeError ("invalid index in Merkle branch")

  def findBitcoinAuxpow (self, blockHash):
    """
    Proceed through blockchain until a block is found that has an auxpow
    which appears also in the Bitcoin blockchain.
    """

    while True:
      blockData = self.nmc.getBlockJson (blockHash)
      assert blockData['hash'] == self.hasher.getRevHex ()

      if 'auxpow' in blockData:
        parent = blockData['auxpow']['parentblock']
        parent = bytearray (binascii.unhexlify (parent))
        parentHash = preamble.doubleHash (parent)
        parentHash = preamble.reverseHex (binascii.hexlify (parentHash))

        res = self.btc.getBlockJson (parentHash, False)
        if (res is not None) and res['confirmations'] > -1:
          return parentHash

      if 'nextblockhash' not in blockData:
        raise RuntimeError ("no link to Bitcoin blockchain found")
      blockHash = blockData['nextblockhash']

      nextHeader = self.nmc.getBlockHeader (blockHash)
      self.buildKnownValue (nextHeader)
      if blockHash != self.hasher.getRevHex ():
        raise RuntimeError ("next block does not contain correct prev link")

  def writeState (self, label):
    """
    Write out code to print the current hashing state with the given label.
    """

    escaped = label.encode ('string_escape')
    self.out.write ("  print '%s: %%s' %% h.getRevHex ()\n" % escaped)

  def writeConstants (self, var):
    """
    Write out the definition of the constants array in the end.
    """

    self.out.write ("%s = []\n" % var)
    for s in self.constants:
      self.out.write ("%s.append ('%s')\n" % (var, s.encode ('string_escape')))

  def generateProof (self, txid):
    """
    Write a proof script for the given txid to the output stream.
    This is the main routine.
    """

    # Fetch original transaction to link to the Bitcoin blockchain.
    txData = self.nmc.getTransactionJson (txid)
    if txData is None:
      raise RuntimeError ("error fetching input tx '%s'" % txid)

    # Copy preamble to output.
    preambleFile = os.path.join (getScriptDir (), "preamble.py")
    with open (preambleFile, "r") as f:
      for line in f:
        self.out.write (line)

    # Find the name's value, which is the ultimately first thing that
    # enters into the hash chain.  This is what should be "proved".
    txHex = self.nmc.getTransactionHex (txid)
    assert txHex is not None
    nameValue = None
    for txOut in txData['vout']:
      if 'nameOp' in txOut['scriptPubKey']:
        if nameValue is not None:
          raise RuntimeError ("multiple name operations found in tx")
        op = txOut['scriptPubKey']['nameOp']
        if 'value' not in op:
          raise RuntimeError ("no value in name operation")
        nameValue = str (op['value'])
    if nameValue is None:
      raise RuntimeError ("no name operation found in tx")

    # Write out the proof-function's header.  Since we want to define
    # all constants later in a "data array", we write the proof out
    # as a function.  That way, we can write it already now and have the
    # data stuff be added only later.
    self.out.write ("\ndef proof (value, c):\n")
    self.out.write ("  h = ChainHasher (value)\n")
    self.out.write ("  print 'Proving value:\\n%s\\n' % value\n")
    self.hasher = preamble.ChainHasher (nameValue)

    # Build the full tx from that.  This gives the txid in the chain hasher.
    self.buildKnownValue (txHex)
    if self.hasher.getRevHex () != txid:
      raise RuntimeError ("could not reproduce original txid")
    self.writeState ("Original txid")

    # Follow the Merkle branch up to the Namecoin block header.
    blockData = self.nmc.getBlockJson (txData['blockhash'])
    reversedTx = map (preamble.reverseHex, blockData['tx'])
    self.contractMerkleTree (reversedTx)
    if self.hasher.getRevHex () != blockData['merkleroot']:
      raise RuntimeError ("count not reproduce Merkle root")

    # Get block and hash it.
    blockHeader = self.nmc.getBlockHeader (txData['blockhash'])
    self.buildKnownValue (blockHeader)
    if self.hasher.getRevHex () != txData['blockhash']:
      raise RuntimeError ("count not reproduce Namecoin block hash")
    self.writeState ("Namecoin block")
    ts = formatTimestamp (blockData['time'])
    sys.stderr.write ("Stamped in Namecoin blockchain: %s\n" % ts)

    # Find block with auxpow linking to the Bitcoin blockchain.
    btcBlockHash = self.findBitcoinAuxpow (self.hasher.getRevHex ())

    # Get auxpow and follow the chain merkle branch.
    blockData = self.nmc.getBlockJson (self.hasher.getRevHex ())
    assert 'auxpow' in blockData
    auxpow = blockData['auxpow']
    branch = map (preamble.reverseHex, auxpow['chainmerklebranch'])
    self.followMerkleBranch (auxpow['chainindex'], branch)

    # What we get should be in the coinbase tx.
    self.out.write ("  h.reverse ()\n")
    self.hasher.reverse ()
    self.buildKnownValue (auxpow['tx']['hex'])

    # Verify the coinbase tx' Merkle branch.
    branch = map (preamble.reverseHex, auxpow['merklebranch'])
    self.followMerkleBranch (auxpow['index'], branch)

    # Finally build up the Bitcoin block.
    btcBlock = self.btc.getBlockHeader (btcBlockHash)
    self.buildKnownValue (btcBlock)
    self.writeState ("Bitcoin block")

    # Final sanity check and status report for the Bitcoin block found.
    assert btcBlockHash == self.hasher.getRevHex ()
    btcBlock = self.btc.getBlockJson (btcBlockHash)
    assert btcBlock['confirmations'] > -1
    ts = formatTimestamp (btcBlock['time'])
    sys.stderr.write ("Stamped in Bitcoin blockchain: %s\n" % ts)

    # Finish the code up.
    self.out.write ("\nvalue = '%s'\n\n" % nameValue.encode ('string_escape'))
    self.writeConstants ("data")
    self.out.write ("\nproof (value, data)\n")

################################################################################

# We expect the desired txid (of the timestamping tx in Namecoin)
# as command-line argument.
if len (sys.argv) != 2:
  sys.stderr.write ("USAGE: namestamp.py TXID\n")
  sys.exit (1)
txid = sys.argv[1]

# Run the main routine.
main = NameStamp (sys.stdout, btc, nmc)
try:
  main.generateProof (txid)
except RuntimeError as exc:
  sys.stderr.write ("Error: %s\n" % exc.message)
  sys.exit (1)
