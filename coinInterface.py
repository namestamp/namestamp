#!/usr/bin/env python

#   Namestamp - link Namecoin timestaps to the Bitcoin blockchain.
#   Copyright (C) 2015  Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import httplib
import json

class CoinInterface:
  """
  Encapsulate the API interface to the Bitcoin or Namecoin daemon.
  It supports all necessary queries, which is for block headers
  and transactions in JSON/HEX.

  The queries are done using the new REST API, so that no credentials
  are necessary.
  """

  def __init__ (self, host, port):
    """
    Initialise the connection, using the passed host and port
    to talk to the daemon.
    """

    self.host = host
    self.port = port

  def getTransactionJson (self, txid):
    """
    Query for a transaction and return its JSON.
    Returns None if no such tx is found.
    """

    res = self._queryHttpGet ("/rest/tx/%s.json" % txid)
    if res is None:
      return None

    return json.loads (res)

  def getTransactionHex (self, txid):
    """
    Query for a transaction and return its raw hex.
    """

    return self._queryHttpGet ("/rest/tx/%s.hex" % txid)

  def getBlockJson (self, blockHash, throw = True):
    """
    Query for a block (without tx details) and return the resulting JSON.
    """

    res = self._queryHttpGet ("/rest/block/notxdetails/%s.json" % blockHash)
    if res is None:
      if throw:
        raise RuntimeError ("could not get block: %s" % blockHash)
      return None

    return json.loads (res)

  def getBlockHeader (self, blockHash, throw = True):
    """
    Query for a block header as raw hex and return it.
    """

    res = self._queryHttpGet ("/rest/headers/1/%s.hex" % blockHash)
    if res is None or res == "":
      if throw:
        raise RuntimeError ("could not get block header: %s" % blockHash)
      return None

    if len (res) < 160:
      raise RuntimeError ("block header is too short")
    return res[0 : 160]

  def _queryHttpGet (self, path):
    """
    Perform a GET request to the given path.  If we get a non-200
    response (e. g., 404), return None.
    """

    conn = httplib.HTTPConnection (self.host, self.port)
    conn.request ("GET", path)

    res = conn.getresponse ()
    if res.status != 200:
      return None

    return res.read ().strip ()
