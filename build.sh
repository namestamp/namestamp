#!/bin/sh

#   Namestamp - link Namecoin timestaps to the Bitcoin blockchain.
#   Copyright (C) 2015  Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Build an archive of the code.

version="0.1"
name="namestamp-$version"

rm -rf /tmp/$name
mkdir /tmp/$name
cp COPYING README NEWS /tmp/$name
cp coinInterface.py preamble.py namestamp.py /tmp/$name
cp -r Demo/ /tmp/$name

fullname="`pwd`/$name.tar.bz2"
rm -f $fullname
(cd /tmp; tar jcvf $fullname $name)
rm -rf /tmp/$name
